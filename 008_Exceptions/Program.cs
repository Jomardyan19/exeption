﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _008_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                throw new Exception();
                //Method(0);
            }
            catch (Exception ex)
            {
                //while (true)
                //{

                //}
                //throw new Exception();
                Method(10);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("My Finally.");
            }

            Console.ReadLine();
        }

        static void Method(int i)
        {
            Console.WriteLine(i);
            Method(++i);
        }
    }
}
