﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var list1 = CreateList(1000);
            var list = new List<int>();

            DateTime dt = DateTime.Now;
            foreach (string item in list1)
            {
                list.Add(item.ToInt32());
            }
            Console.WriteLine(DateTime.Now - dt);
            Console.ReadLine();
        }

        static List<string> CreateList(int count)
        {
            var list = new List<string>(count);
            Random rnd = new Random();
            for (int i = 0; i < count; i+=2)
            {
                list.Add(rnd.Next().ToString());
                list.Add(null);
            }

            return list;
        }
    }

    static class Ext
    {
        public static int ToInt32(this string item)
        {
            //if (int.TryParse(item, out int el))
            //    return el;
            //return 0;
            int.TryParse(item, out int el);
            return el;

            //try
            //{
            //    return int.Parse(item);
            //}
            //catch
            //{
            //    return 0;
            //}
        }
    }
}
