﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _018_using
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var my = new MyClass())
            {
                
            }

            //MyClass my1 = null;
            //try
            //{
            //    my1 = new MyClass();
            //}
            //finally
            //{
            //    my1.Dispose();
            //}

            Console.ReadLine();
        }
    }

    class MyClass : IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine("My class disposed.");
        }
    }
}
