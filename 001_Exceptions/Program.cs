﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_Exceptions
{
    //https://msdn.microsoft.com/en-us/library/5whzhsd2(v=vs.110).aspx
    //https://msdn.microsoft.com/en-us/library/system.systemexception(v=vs.110).aspx

    class Program
    {
        static void Main(string[] args)
        {
            int n = 2;
            try
            {
                int a = 2 / (2 - n);

                Console.WriteLine("Test Message 1.");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"[Artyom] {ex.Message}");
                Console.WriteLine("Texi e unecel sxal. Dimeq dzer administartorin.");
                //string text = ex.Message;
            }

            Console.WriteLine("Test Message 2.");

            Console.ReadLine();
        }
    }
}
