﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicFramework.Tests
{
    [TestClass()]
    public class MicMathTests
    {
        [TestMethod()]
        public void MaxTest()
        {
            int[] arr = { 1000, -250, 100, 1 };
            int max = MicMath.Max(arr);
            Assert.IsTrue(max == 1000);

            arr = new int[] { 0, 0, 0, 0 };
            max = MicMath.Max(arr);
            Assert.IsTrue(max == 0);

            arr = new int[] { -1, -2, -100, -8 };
            max = MicMath.Max(arr);
            Assert.IsTrue(max == -1);

            arr = new int[0];
            //int max = MicMath.Max(arr);
            Assert.ThrowsException<IndexOutOfRangeException>(() => MicMath.Max(arr));

            arr = null;
            Assert.ThrowsException<NullReferenceException>(() => MicMath.Max(null));
        }
    }
}