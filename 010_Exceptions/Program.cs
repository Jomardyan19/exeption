﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _010_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception("Exception from Catch.");

                //while (true)
                //    Console.WriteLine("Catch.");
            }
            finally
            {
                Console.WriteLine("Finally.");
            }

            Console.ReadLine();
        }
    }
}