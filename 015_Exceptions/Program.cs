﻿using _015_Exceptions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _015_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nums = CreateRandomNumArr(1000);

            DateTime now = DateTime.Now;
            for (int i = 0; i < nums.Length; i++)
            {
                int num = nums[i].TryCatchToInt32();
            }

            Console.Write("Try-Catch: ");
            Console.WriteLine((DateTime.Now - now).ToString(@"ss\.fffffff"));

            Console.WriteLine(new string('*', 20));

            now = DateTime.Now;
            for (int i = 0; i < nums.Length; i++)
            {
                int num = nums[i].ToInt32();
            }

            Console.Write("TryParse: ");
            Console.WriteLine((DateTime.Now - now).ToString(@"ss\.fffffff"));

            Console.ReadLine();
        }

        static string[] CreateRandomNumArr(int count)
        {
            var arr = new string[count];
            var rnd = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rnd.Next(0, 10000).ToString();
                //arr[i] = "hhhjhjg";
            }

            return arr;
        }
    }
}