﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_Exceptions
{
    class MyClass
    {
        public void Method()
        {
            Exception ex = new Exception("My exception");
            ex.HelpLink = "http://mic.am/ErrorService";
            ex.Data.Add("Reason", "Test Exception");
            ex.Data.Add("Date", DateTime.Now);

            throw ex;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass instance = new MyClass();
            try
            {
                instance.Method();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.TargetSite);
                Console.WriteLine(ex.TargetSite.DeclaringType);
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.HelpLink);
                Console.WriteLine(ex.StackTrace);

                foreach (DictionaryEntry item in ex.Data)
                {
                    Console.WriteLine("{0} : {1}", item.Key, item.Value);
                }
            }

            Console.ReadLine();
        }
    }
}