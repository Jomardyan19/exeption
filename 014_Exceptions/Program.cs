﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _014_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var my = new MyClass())
            {
                // ...
            }

            #region Using Block

            MyClass my1 = null;
            try
            {
                my1 = new MyClass();
            }
            finally
            {
                if (my1 != null)
                    my1.Dispose();
            }

            #endregion

            Console.ReadLine();
        }
    }

    class MyClass : IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine("Disposed");
        }
    }
}