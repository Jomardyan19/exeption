﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            MyClass1 my1 = new MyClass1();
            Console.WriteLine(my1?.MyClass2?.MyClass3?.field3);
            //if(my1 != null)
            //{
            //    if(my1.MyClass2 != null)
            //    {
            //        if(my1.MyClass2.MyClass3 != null)
            //        {
            //            Console.WriteLine(my1.MyClass2.MyClass3.field3);
            //        }
            //    }
            //}
        }
    }

    class MyClass1
    {
        public int field1;
        public MyClass2 MyClass2 { get; set; }
    }

    class MyClass2
    {
        public int field2;
        public MyClass3 MyClass3 { get; set; }
    }

    class MyClass3
    {
        public int field3;
    }
}
