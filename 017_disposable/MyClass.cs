﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _017_disposable
{
    class MyClass : IDisposable
    {
        public void Dispose()
        {
            Console.WriteLine("Disposed....");
        }
    }
}
