﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _006_Exceptions
{
    public class MicWriter1 : StreamWriter
    {
        public MicWriter1(Stream stream) : base(stream) { }

        public void Writedata(List<string> data)
        {
            foreach (string lineData in data)
            {
                WriteLine(lineData);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }

    public class MicWriter : IDisposable
    {
        public MicWriter(string path)
        {
            _writer = File.CreateText(path);
        }

        private StreamWriter _writer;

        public void Writedata(List<string> data)
        {
            foreach (string lineData in data)
            {
                _writer.WriteLine(lineData);
            }
        }

        public void Dispose()
        {
            if (_writer != null)
                _writer.Dispose();
            _writer = null;
        }
    }

    public static class FileManager
    {
        public static void Save2(string path, List<string> data)
        {
            using (MicWriter writer = new MicWriter(path))
            {
                writer.Writedata(data);
            }

            //MicWriter writer = new MicWriter(path);
            //try
            //{
            //    writer.Writedata(data);
            //}
            //finally
            //{
            //    writer.Dispose();
            //}
        }

        public static void Save(string path, List<string> data)
        {
            using (var stream = File.CreateText(path))
            {
                foreach (string lineData in data)
                {
                    stream.WriteLine(lineData);
                }
            }
        }

        public static void Save1(string path, List<string> data)
        {
            var stream = File.CreateText(path);
            try
            {
                foreach (string lineData in data)
                {
                    stream.WriteLine(lineData);
                }
            }
            finally
            {
                stream.Dispose();
            }
        }
    }

    //public static bool Save(string path, List<string> data)
    //{
    //    var stream = File.CreateText(path);
    //    try
    //    {
    //        foreach (string lineData in data)
    //        {
    //            stream.WriteLine(lineData);
    //        }
    //    }
    //    catch
    //    {
    //        return false;
    //    }
    //    finally
    //    {
    //        stream.Dispose();
    //    }

    //    return true;
    //}
}