﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mic.Lesson.NviPattern.TestApp
{
    class BaseRepository
    {
        public List<int> SelectAll()
        {
            try
            {
                return OnSelectAll();
            }
            catch (Exception ex)
            {
                // Write log.
                return null;
            }
        }

        protected virtual List<int> OnSelectAll()
        {
            return new List<int> { 1, 2, 3 };
        }
    }

    class Repository1 : BaseRepository
    {
        protected override List<int> OnSelectAll()
        {
            return new List<int> { 10, 20, 30 };
        }
    }

    class Repository2 : BaseRepository
    {
        protected override List<int> OnSelectAll()
        {
            return new List<int> { 10, 20, 30 };
        }
    }
}
