﻿using System;
using barev = System.Int32;
using generic = System.Collections.Generic;
using static System.Console;

namespace _006_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            WriteLine();

            generic.List<int> list;
            barev a1 = 10;

            string path = @"C:\Users\ar.tonoyan\Desktop\TestDir\test.txt";
            //MicWriter writer = null;
            //try
            //{
            //    writer = new MicWriter(path);

            //    writer.Writedata(null);
            //}
            //catch (Exception ex)
            //{
            //}
            //finally
            //{
            //    if (writer != null)
            //        writer.Dispose();
            //}

            using (MicWriter writer = new MicWriter(path))
            {

            }

            int n = 1;
            try
            {
                int a = 10 / (n - 2);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("finally!!!!!");
            }

            Console.ReadLine();
        }
    }
}
