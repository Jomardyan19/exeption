﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _004_Exceptions
{
    class MyException : Exception
    {
        public MyException() : base("My Message") { }

        public void Method()
        {
            Console.WriteLine("My Custom Exception.");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                throw new MyException();
            }
            catch (MyException ex)
            {
                ex.Method();
            }

            Console.ReadLine();
        }
    }
}