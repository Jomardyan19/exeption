﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _005_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                throw new Exception();
            }
            catch (Exception ex)
            {
                try
                {
                    try
                    {
                        throw new Exception();
                    }
                    catch (Exception ex1)
                    {
                    }
                }
                catch (Exception ex1)
                {
                }
            }
        }
    }
}
