﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            object obj = null;
            try
            {
                int[] arr = null;
                GetMax(arr);

                if (obj == null)
                    throw new Exception("Ha ha ha!!!");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }

        static int GetMax(int[] arr)
        {
            if (arr == null)
                throw new Exception("Array is null");

            Method(arr);
            //try
            //{
            //    Method(arr);
            //}
            //catch (Exception ex)
            //{
            //}

            return arr.Max();
        }

        static void Method(int[] arr)
        {
            if (arr == null)
                throw new Exception("Array is null");

            throw new Exception("Array is null");
        }
    }
}
