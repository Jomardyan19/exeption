﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_Exceptions
{
    public class MyClass
    {
        public void ThrowInner()
        {
            throw new Exception("Inner Exception.");
        }

        public void CatchInner()
        {
            try
            {
                ThrowInner();
            }
            catch (Exception ex)
            {
                throw new Exception("Out Exception", ex);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            MyClass my = new MyClass();
            try
            {
                my.CatchInner();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException.Message);
            }

            Console.ReadLine();
        }
    }
}